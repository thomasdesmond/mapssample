﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MapSampleCS481
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();

            PopulatePicker();
        }

        private void PopulatePicker()
        {
            Dictionary<string, Color> nameToColor = new Dictionary<string, Color>()
            {
                { "Aqua", Color.Aqua },
                { "Gray", Color.Gray },
                { "Lime", Color.Lime },
                { "Navy", Color.Navy },   
                { "Purple", Color.Purple },
                { "Silver", Color.Silver },
                { "White", Color.White }, 
            };


            foreach (var item in nameToColor)
            {
                SamplePicker.Items.Add(item.Key);
            }

            SamplePicker.SelectedIndex = 0;
        }
        void Handle_NavigateToMapSample(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MapSamplePage());
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // Code to handle user making index changes in picker
        }
    }
}
